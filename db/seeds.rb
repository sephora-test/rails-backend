# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#

unless Content.exists?
  (1..20).each do
    print 'Creating Content...'
    FactoryBot.create(:content)
    puts 'Done!'
  end
end
