class CreateContents < ActiveRecord::Migration[5.2]
  def change
    create_table :contents do |t|
      t.string :title
      t.string :author
      t.string :summary
      t.text :content
      t.datetime :published_date
      t.integer :state, default: 0

      t.timestamps
    end
  end
end
