# == Schema Information
#
# Table name: contents
#
#  id             :integer          not null, primary key
#  title          :string
#  author         :string
#  summary        :string
#  content        :text
#  published_date :datetime
#  state          :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

FactoryBot.define do
  factory :content do
    title { Faker::Lorem.sentence }
    author { Faker::Name.name }
    content { Faker::Lorem.paragraph }
    published_date { Faker::Date.forward(1) }
    summary { Faker::Lorem.sentence }
    state { [:draft, :published].sample }
  end
end
