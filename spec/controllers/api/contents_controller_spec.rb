require 'rails_helper'

RSpec.describe Api::ContentsController, type: :request do
  let(:auth_token) { Rails.application.secrets.auth_token }
  let!(:content) { FactoryBot.create(:content, state: :published) }
  let(:headers) { { "Authorization" => "Token token=#{auth_token}" } }

  describe 'Invalid request' do
    def do_request
      headers = { "Authorization" => "Token token=#{Faker::Lorem.word}" }
      get '/api/contents', headers: headers
    end

    describe 'return list of content' do
      before { do_request }

      it { expect_json({ success: false }) }
      it { expect_json('error', "You're not authenticated") }
    end
  end

  describe 'GET #index' do
    def do_request
      get '/api/contents', headers: headers
    end

    describe 'return list of content' do
      before { do_request }

      it { expect_status 200 }
      it { expect_json({ success: true }) }
      it { expect_json('data.0', { content: content.content, id: content.id }) }
    end
  end

  describe 'GET #show' do
    context 'return content detail' do
      def do_request
        get "/api/contents/#{content.id}", headers: headers
      end
      before { do_request }

      it { expect_status 200 }
      it { expect_json({ success: true }) }
      it { expect_json('data', { content: content.content, id: content.id }) }
    end

    context 'fail to request' do
      def do_request
        get "/api/contents/#{Faker::Number.number}", headers: headers
      end

      before { do_request }

      it { expect_json({ success: 'error' }) }
    end
  end
end
