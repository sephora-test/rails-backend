# == Schema Information
#
# Table name: contents
#
#  id             :integer          not null, primary key
#  title          :string
#  author         :string
#  summary        :string
#  content        :text
#  published_date :datetime
#  state          :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'rails_helper'

RSpec.describe Content, type: :model do
  describe 'factories' do
    subject { FactoryBot.create(:content) }
    it { should be_valid }
  end

  describe 'validation' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:author) }
    it { should validate_presence_of(:content) }
    it { should validate_presence_of(:state) }
  end
end
