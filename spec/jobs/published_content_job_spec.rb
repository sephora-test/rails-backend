require 'rails_helper'
include ActiveJob::TestHelper

RSpec.describe PublishedContentJob, type: :job do
  let!(:content) { FactoryBot.create(:content, state: :draft) }
  subject(:job) { described_class.perform_later() }

  describe 'run the job' do
    it 'enqueueed the job' do
      ActiveJob::Base.queue_adapter = :test
      expect { job }.to have_enqueued_job(described_class)
        .on_queue('default')
    end
  end
end
