class Api::ContentsController < Api::BaseController
  before_action :set_paginate, only: %w(index)
  before_action :get_content, only: %w(show)

  api :GET, '/contents/', "Show all contents published"
  def index
    @contents = Content.published.page(@page).per(@per)
    success_json(@contents, 'content', @per, @page, @contents.total_pages,
      api_contents_url(per: @per, page: @page), api_contents_url(per: @per, page: @page.to_i + 1),
      api_contents_url(per: @per, page: @contents.total_pages))
  end

  api :GET, '/contents/:id', 'Show content with id'
  param :id, :number, desc: 'Content ID', required: true
  def show
    show_success_json(@content, 'content')
  end

  private

  def get_content
    @content = Content.find(params[:id])
  end

  def content_params
    params.require(:content).permit(
      :title,
      :author,
      :content,
      :summary,
      :state,
      :published_date
    )
  end
end
