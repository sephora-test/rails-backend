class Api::BaseController < ApplicationController
  include ActionController::Serialization
  include Api::BaseHelper

  rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity_response
  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response
  rescue_from ActionController::RoutingError, with: :render_not_found
  rescue_from StandardError, with: :render_internal_error
  protect_from_forgery with: :null_session
  before_action :authenticate_from_token!

  def render_unprocessable_entity_response(exception)
    message_json(:unprocessable_entity, exception.record.errors)

  end

  def render_not_found_response(exception)
    message_json(:not_found, exception.message)
  end

  def render_internal_error(exception)
    message_json(:error, exception.message)
  end

  def render_not_found(exception)
    message_json(:not_found, exception.message)
  end

  def set_paginate
    @page = params[:page] || 1
    @per = params[:per] || 5
  end

  def current_user
    @user
  end

  def message_json(status, message)
    render json: {
      success: status,
      message: message
    }
  end

  def success_json(object, key, per, page, total_pages, current_path, next_path, last_path)
    serializer = key.camelize + 'Serializer'
    render json: {
      success: true,
      links: link_json(current_path, next_path, last_path),
      data: object.map { |x|
        serializer.constantize.new(x)
      },

      paginate: { per: per, page: page, total_pages: total_pages }
    }
  end

  def link_json(current_path, next_path, last_path)
    { self: current_path,
      next: next_path,
      last: last_path }
  end

  def show_success_json(object, key)
    serializer = key.camelize + 'Serializer'
    render json: {
      success: true,
      data: serializer.constantize.new(object)
    }
  end

  def raise_invalid_message(object, api_error)
    raise api_error.constantize, object.errors.full_messages.to_sentence
  end
end
