# == Schema Information
#
# Table name: contents
#
#  id             :integer          not null, primary key
#  title          :string
#  author         :string
#  summary        :string
#  content        :text
#  published_date :datetime
#  state          :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Content < ApplicationRecord
  enum state: [:draft, :published]
  # Validations
  validates_presence_of :title, :author, :content, :state

  scope :published, -> { where(state: :published) }
  scope :draft, -> { where(state: :draft) }
end
