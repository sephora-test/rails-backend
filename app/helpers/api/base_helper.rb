module Api::BaseHelper
  def authenticate_from_token!
    auth_token = params[:auth_token].presence || ActionController::HttpAuthentication::Token.token_and_options(request).try(:first)
    render json: { success: false, error: "You're not authenticated" }, status: :not_authenticated if auth_token != Rails.application.secrets.auth_token
  end
end
