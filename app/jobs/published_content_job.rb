class PublishedContentJob < ApplicationJob
  queue_as :default

  def perform(*args)
    Content.draft.find_in_batches do |content|
      content.map{ |c| c.update_columns(state: :published, published_date: Time.now) }
    end
  end
end
