class BaseSerializer < ActiveModel::Serializer
  # serialization_scope :current_user

  attributes :type,
             :id,
             :created_at,
             :updated_at

  def type
    object.class.name.pluralize
  end
end
