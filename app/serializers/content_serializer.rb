# == Schema Information
#
# Table name: contents
#
#  id             :integer          not null, primary key
#  title          :string
#  author         :string
#  summary        :string
#  content        :text
#  published_date :datetime
#  state          :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class ContentSerializer < BaseSerializer
  attributes :title,
             :author,
             :summary,
             :content,
             :state,
             :published_date
end
