Apipie.configure do |config|
  config.app_name                = "SephoraTest"
  config.api_base_url            = "/api"
  config.doc_base_url            = "/apipie"
  config.default_locale          = nil
  config.translate               = false
  # where is your API defined?
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/**/*.rb"
end
