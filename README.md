# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
`2.5.0`
* Rails version
`5.2.0.beta2`

* Configuration
`cp config/database.yml.sample config/database.yml`
`cp .env.sample .env` - Config the auth token key here. This key use for request API

* Database creation
`bundle exec rake db:create`

* Database initialization
`bundle exec rake db:migrate`
`bundle exec rake db:seed`

* How to run the test suite
`bundle exec rspec spec/`

* Services (job queues, cache servers, search engines, etc.)
Run `PublishedContentJob.perform_later` to published all draft contents

To run job published content I use whenever gem, set the background job run once a day. You can check config/schedule.rb

* Heroku
Frontend: https://sephora-test-frontend.herokuapp.com/
Backend: https://sephora-test-api.herokuapp.com/
* ...
